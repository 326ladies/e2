package MuTorere; 

import MuTorere.Player;
import java.util.Scanner; 
 
public class HumanPlayer extends Player { 
  
  
 /** 
 * Constructor 
 * boardReader provides access to the current state of the game 
 * playerID determines whether you are player 1 or 2. 
 * @param boardReader provides access to the current board state. 
  *@param playerID player ID, either Board.Piece.ONE or Board.Piece.TWO 
 */ 
 public HumanPlayer(BoardReader boardReader, Board.Piece playerID) { 
  super(boardReader, playerID);
 } 
  
 /** 
 * Need to implement this. 
 * Return the index of the piece that you want to move. 
 *  If the result is not a valid move, you lose. 
 * If there are no valid moves, just return something - don't leave us hanging! 
 * @return The location of the piece that you wish to move to the empty space.
 *    7   0
 *  6       1
 *      8
 *  5       2
 *    4   3
 */ 
 public int getMove() {

  Scanner sc = new Scanner(System.in);
  int move = sc.nextInt();

  return move;
}
 	  

}
