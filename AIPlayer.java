package MuTorere; 

import MuTorere.Player; 
import java.util.Random;

public class AIPlayer extends Player { 
  
  
  /** 
   * Constructor 
   * boardReader provides access to the current state of the game 
   * playerID determines whether you are player 1 or 2. 
   * @param boardReader provides access to the current board state. 
   *@param playerID player ID, either Board.Piece.ONE or Board.Piece.TWO 
   */ 
  public AIPlayer(BoardReader boardReader, Board.Piece playerID) { 
    super(boardReader, playerID);
  } 
  
  /** 
   * Need to implement this. 
   * Return the index of the piece that you want to move. 
   * If the result is not a valid move, you lose. 
   * If there are no valid moves, just return something - don't leave us hanging! 
   * @return The location of the piece that you wish to move to the empty space.
   */ 
  public int getMove() {
    
    int blankSpace = -1;
    int idealMove = -1;
    
    //Find the blank space
    for (int i = 0; i <= 8; i++) {
      if (boardReader.pieceAt(i) == Board.Piece.BLANK) {
        blankSpace = i;
        break;
      }
    }
    
    // To move into the Centre
    if (blankSpace == 8) {
      int movesFound = 0;
      int weight = 0;
      
      int numberPossible = 0;
      int[] allPossibleMoves = new int[8];
      
      
      for (int i = 0; i <= 8; i++) {
        if (boardReader.pieceAt(i) == playerID) { 
          if (canMove(i, blankSpace)) {
            allPossibleMoves[numberPossible] = i;
            numberPossible++;
            
            int[] prevNeighbours = findPrevNeighbours(i);
            int[] nextNeighbours = findNextNeighbours(i);
            
            if ((boardReader.pieceAt(prevNeighbours[0]) == playerID) && (boardReader.pieceAt(prevNeighbours[1]) != playerID)) {
              // Look out for the BIG TRAP i.e. if you have them 3 on one side, 1 on the other move in your token near to their 1
              if ((boardReader.pieceAt(nextNeighbours[0]) != playerID) && (boardReader.pieceAt(nextNeighbours[1]) == playerID)) {
                // Need to look at further neghbours to double check we are in big trap set up
                int[] deepNextNeighbours = findNextNeighbours(nextNeighbours[1]);
                if (boardReader.pieceAt(deepNextNeighbours[0]) == playerID) {
                  if (4 > weight) {
                    idealMove = i;
                    weight = 4;
                    movesFound++;
                  }
                }
              }
            } else if ((boardReader.pieceAt(prevNeighbours[0]) != playerID) && (boardReader.pieceAt(prevNeighbours[1]) == playerID)) {
              // Symmetrical case to above
              if ((boardReader.pieceAt(nextNeighbours[0]) == playerID) && (boardReader.pieceAt(nextNeighbours[1]) != playerID)) {
                int[] deepPrevNeighbours = findPrevNeighbours(nextNeighbours[1]);
                if (boardReader.pieceAt(deepPrevNeighbours[0]) != playerID) {
                  //10
                  if (4 > weight) {
                    idealMove = i;
                    weight = 4;
                    movesFound++;
                  }
                }
              }           
            } else if ((boardReader.pieceAt(prevNeighbours[0]) == playerID) && (boardReader.pieceAt(prevNeighbours[1]) == playerID)) {
            	// If your two RHS neighbours are both your own, pick one and move that one to the middle to separate your three
              if (2 > weight) {
                weight = 2;
                Random r = new Random();
        		int n = r.nextInt(2);
        		if (n == 0) {
                	idealMove = i;
                } else {
                	if (canMove(prevNeighbours[1], blankSpace)) {
                		idealMove = prevNeighbours[1];
                	} else {
                		int betterMove = prevNeighbours[1] - 1;
                		if (betterMove < 0) {
                			betterMove = 7;
                		}
                		idealMove = betterMove;
                	}
                }
                movesFound++;
              }
            } else {
              if ((boardReader.pieceAt(prevNeighbours[0]) != playerID) && (boardReader.pieceAt(nextNeighbours[0]) != playerID)) {
                // If both Left neighbour and Right neighbour to your potential move are your opponent
                if ((boardReader.pieceAt(prevNeighbours[1]) != playerID) | (boardReader.pieceAt(nextNeighbours[1]) != playerID)) {
                  // If one further neighbour to either of the above neighbours is also your opponent
                  // So that the opponent is able to move into a group of three i.e. lead them closer together
                  if (1 > weight) {
                    weight = 1;
                    idealMove = i;
                    movesFound++;
                  }
                } else if ((boardReader.pieceAt(prevNeighbours[1]) == playerID) && (boardReader.pieceAt(nextNeighbours[1]) == playerID)) {
                  // Or if both further neighbours are both you, then move this one
                  if (3 > weight) {
                    weight = 3;
                    idealMove = i;
                    movesFound++;
                  }
                }
              }
            }
          }
        }
      }
      
      // If no ideal move found to centre, just move any you can
      if (movesFound == 0) {
        Random r = new Random();
        int n = r.nextInt(numberPossible);
        idealMove = allPossibleMoves[n];
        
      }
    } 
    
    
    // To move to one of the Kawai Points
    if (blankSpace != 8) {
      // Set all the neighbours
      int prev = blankSpace - 1;
      if (prev < 0) prev = 7;
      int next = blankSpace + 1;
      if (next > 7) next = 0;
      
      int[] prevNeighbours = findPrevNeighbours(prev);
      int[] nextNeighbours = findNextNeighbours(next);
      
      boolean twoOptions = false;
      int movesFound = 0;
      
      
      // If both two kawai next to the blank are your own, set two options to true
      if ((boardReader.pieceAt(prev) == playerID) && (boardReader.pieceAt(next) == playerID)) {
        twoOptions = true;
      }
      
      if (twoOptions) {
        // Pick the better of two options that are available
        if (boardReader.pieceAt(8) == playerID) {
        	//If one move away from setting up the big trap, move the centre
        	// This is i.e. to create group of three and 1 of each player, with opponents next move ONLY able to be the middle
        	if ((boardReader.pieceAt(prevNeighbours[0]) != playerID) && (boardReader.pieceAt(prevNeighbours[1]) != playerID)) {
        		if ((boardReader.pieceAt(nextNeighbours[0]) != playerID) && (boardReader.pieceAt(nextNeighbours[1]) == playerID)) {
        			idealMove = 8;
            		movesFound++;
        		}
        	} else if ((boardReader.pieceAt(prevNeighbours[0]) != playerID) && (boardReader.pieceAt(prevNeighbours[1]) == playerID)) {
        		if ((boardReader.pieceAt(nextNeighbours[0]) != playerID) && (boardReader.pieceAt(nextNeighbours[1]) != playerID)) {
        			idealMove = 8;
            		movesFound++;
        		}
        	}
        } else if ((boardReader.pieceAt(prevNeighbours[0])!= playerID) && (boardReader.pieceAt(prevNeighbours[1])!= playerID)) {
          if (boardReader.pieceAt(8) != playerID) {
            //Move the right token if your left has two opponent neighbours, only if the middle is not yours
            idealMove = next;
            movesFound++;
          }
        } else if ((boardReader.pieceAt(nextNeighbours[0])!= playerID) && (boardReader.pieceAt(nextNeighbours[1])!= playerID)) {
          //Move the left token if your right has two opponent neighbours, only if the middle is not yours
          if (boardReader.pieceAt(8) != playerID) {
            idealMove = prev;
            movesFound++;
          }
        } else if ((boardReader.pieceAt(prevNeighbours[0]) == playerID) && (boardReader.pieceAt(prevNeighbours[1]) == playerID)) {
          // If you have a group of three tokens together, move the further one to lead into the Big Trap i.e. away from your group of three
          idealMove = prev;
          movesFound++;
        } else if ((boardReader.pieceAt(nextNeighbours[0])== playerID) && (boardReader.pieceAt(nextNeighbours[1])== playerID)) {
          //If you have a group of three tokens together, move the further one to lead into the Big Trap i.e. away from your group of three
          idealMove = next;
          movesFound++;
        } else {
          // Otherwise just move the one of you that's closest to your own token
          if (boardReader.pieceAt(nextNeighbours[0]) == playerID) {
            idealMove = next;
            movesFound++;
          } else if (boardReader.pieceAt(prevNeighbours[0]) == playerID) {
            idealMove = prev;
            movesFound++;
          } else {
            //Randomly choose either one to move if no better cases were found.....
            Random r = new Random();
            int n = r.nextInt(1);
            if (n == 0) {
              idealMove = prev;
            } else {
              idealMove = next;
            }
            movesFound++;
          }
        }
      } else {
        if (boardReader.pieceAt(prev) == playerID) {
          idealMove = prev;
          movesFound++;
        } else if (boardReader.pieceAt(next) == playerID){
          idealMove = next;
        }
      }
      //Otherwise move out from the inside
      if (boardReader.pieceAt(8) == playerID) {
        if (movesFound == 0) {
          idealMove = 8;
        } 
      }
    }
    
    return idealMove;
  } 
  
  
  /** 
   * Determine whether moving a particular index is a legal move or not.
   * @param index the number of the potential move you are checking.
   * @param blankLocation the index of the blank location on the board.
   * @return true if the move is legal and valid.
   */
  public boolean canMove(int index, int blankLocation) {
    if (blankLocation == 8) {
      // Move to centre, check for valid neighbour
      int prev = index - 1;
      if (prev < 0) prev = 7;
      int next = index + 1;
      if (next > 7) next = 0;
      if (boardReader.pieceAt(prev) == playerID && boardReader.pieceAt(next) == playerID) {
        return false;
      }
    } else {
      // Either move from centre to kewai...
      if (index == 8) {
        return true;
      }
      // ... or from one kewai to next, make sure they are neighbours
      int prev = index - 1;
      if (prev < 0) prev = 7;
      int next = index + 1;
      if (next > 7) next = 0;
      if (prev != blankLocation && next != blankLocation) {
        return false;
      }   
    }
    return true;
  }
  
  /**
   * Determine the indexes of both backward neighbouring kawai to a specific index.
   * That is, determind two the positions two behind the index.
   * @param index the kawai number to find neighbours for.
   * @return the two neighbours in an array.
   */
  public int[] findPrevNeighbours(int index) {
    //prev[0] = RIGHT neighbours and prev[1] == RIGHT RIGHT neighbour 
    // i.e index = 4. prev[0] = 3, prev[1] = 2
    int[] prevNeighbours = new int[2];
    
    prevNeighbours[0] = index - 1;
    if (prevNeighbours[0] < 0) {
      prevNeighbours[0] = 7;
    }
    prevNeighbours[1] = prevNeighbours[0] - 1;
    if (prevNeighbours[1] < 0) {
      prevNeighbours[1] = 7;
    }
    
    return prevNeighbours;
  }
  
  
  /** 
   * Determine the indexes of both forward neighbouring kawai to a specific index.
   * That is, determind two the positions two ahead of the index.
   * @param index the kawai number to find neighbours for.
   * @return the two neighbours in an array.
   */
  public int[] findNextNeighbours(int index) {
    //next[0] = left neighbours and next[1] == left left neightbour 
    // i.e index = 4. next[0] = 5, next[1] = 6
    int[] nextNeighbours = new int[2];
    
    nextNeighbours[0] = index + 1;
    if (nextNeighbours[0] > 7) {
      nextNeighbours[0] = 0;
    }
    nextNeighbours[1] = nextNeighbours[0] + 1;
    if (nextNeighbours[1] > 7) {
      nextNeighbours[1] = 0;
    }
    
    return nextNeighbours;
  }
  
  
  
}
